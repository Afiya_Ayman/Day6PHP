<?php

for($i = 1; $i <=10; ++$i)
{
    echo $i;
    echo "<br>";
    break;  //to break the loop


}

echo "<hr>";

$i = 0;
while (++$i) {
    switch ($i) {
        case 5:
            echo "At 5<br />\n";
            break 1;  /* Exit only the switch. */
        case 10:
            echo "At 10; quitting<br />\n";
            break 2;  /* Exit the switch and the while. */
        default:
            break;
    }
}